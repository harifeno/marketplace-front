import { Categorie } from './categorie';
import { Region } from './region';
import { User } from './user';

export class Annonce{
    id:number;
    titre:string;
    texte:string;
    prix:number;
    photo:string;
    date=Date.now();
    contact:string;
    spec1:string;
    spec2:string;
    spec3:string;
    categorie: Categorie;
    region: Region;
    usermanager: User;
   }