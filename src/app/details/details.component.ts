import { Component, OnInit } from '@angular/core';
import { AnnonceService } from '../services/annonce.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  annonceinfos:{};

  constructor(private annonceService:AnnonceService) { }



  ngOnInit() {
    this.annonceService.getAnnonceById(this.annonceService.idAnnonce).subscribe((data: {}) => {
    this.annonceinfos = data;
     });
  }
    
}
