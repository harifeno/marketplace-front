import { Component, OnInit } from '@angular/core';
import { AnnonceService } from '../services/annonce.service';
import { Annonce } from '../annonce';
import { Categorie } from '../categorie';
import { Region } from '../region';
import { LoginService } from '../services/login.service';
import { User } from '../user';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private annonceService:AnnonceService, private loginService:LoginService) { }

  categories:{}; regions:{}
  user:User;
  annonce=new Annonce;categorie=new Categorie;region=new Region;
  
  
  ajouterAnnonce(){
    this.annonce.usermanager=this.user;   
    this.annonceService.addAnnonce(this.annonce).subscribe(
    res=> alert("Votre annonce a été publiée avec succès")
    )
    location.reload()
    console.log(this.annonce)
    
  }

  definirCategorie(){
    console.log(this.annonce.categorie);    
  }

  definirRegion(){
    console.log(this.annonce.region);   
  }
  
  ngOnInit() {
    
this.annonceService.getAllCategories().subscribe((data: {}) => {
      this.categories = data;
    });
this.annonceService.getAllRegions().subscribe((data: {}) => {
      this.regions = data;
    });
this.loginService.getOneUser(localStorage.getItem('username')).subscribe((data: User) => {
      this.user = data;      
    });



  }

}
