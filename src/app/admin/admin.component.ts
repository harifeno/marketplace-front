import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor(private userService : UserService) { }

users:[];

afficherUsers(){
  this.userService.getAllUsers().subscribe((data: []) => {
    this.users = data;
  })
}

supprimerUser(id:number) {
  if (confirm("Voulez vous supprimer cet utilisateur et toutes ses annonces")){
  this.userService.deleteUser(id).subscribe(
    res=>{this.afficherUsers}    
  )}
}

  ngOnInit() {
    this.userService.getAllUsers().subscribe((data: []) => {
      this.users = data;
    })
  
  }

}
