import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  
  
  constructor(private loginService:LoginService) { }

  statut:string="Déconnecté";

  seDeconnecter(){
  localStorage.clear();
  location.reload();
}


  ngOnInit() {
    if(localStorage.getItem("username")!=null){
      this.statut="Connecté"
    }else{this.statut="Déconnecté"};
    
  }

}
