import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  
  returnurl:string;
  private login:string;
  private password:string;
  
  show:boolean=false
  users = {};
  newUser={login:'',password:'',name_user:'',email:'',tel:''};password2:'';

  constructor(private loginService:LoginService,private activatedRoute:ActivatedRoute, private router:Router) { }

  ngOnInit() {
    this.returnurl = this.activatedRoute.snapshot.queryParams['returnurl'] || '/';
  }

seLogger(){
this.loginService.authentication(this.login,this.password).pipe(first()).subscribe(
  data=>{
    console.log('data reponse: ',data);
    localStorage.setItem("token",data.headers.get('Authorization'));
    localStorage.setItem("username",this.login);    

    let jwt = localStorage.getItem("token")
    let jwtData = jwt.split('.')[1]
    let decodedJwtJsonData = window.atob(jwtData)
    let decodedJwtData = JSON.parse(decodedJwtJsonData)
    let roles = decodedJwtData.roles[0].authority
    // console.log('jwtData: ' + jwtData)
    // console.log('decodedJwtJsonData: ' + decodedJwtJsonData)
    // console.log('decodedJwtData: ' + decodedJwtData)
    console.log('admin or user: ' + roles)
    if(roles==="ROLE_ADMIN"){
      
      
      location.reload();
      this.router.navigate(["/admin"])
      
    }
    else if (roles==="ROLE_USER") {
      location.reload();
      this.router.navigate(["/user"])}
  },  
  error=>{
    alert('verifier vos données svp!!');
  }
);
}

ajouterUser(){
  if (this.newUser.password===this.password2){
  this.loginService.addUser(this.newUser).subscribe(res =>{
    alert("Nouvel utilisateur ajouté avec succès")
    document.getElementById('formulaireUser').style.display='none';
  this.newUser.login='';this.newUser.password='';this.newUser.email='';this.newUser.tel='';this.password2='';this.newUser.name_user='';
  });}else{alert("Vérifier les mots de passe que vous avez entrer")}
  this.newUser.password='';this.password2='';
}

}
