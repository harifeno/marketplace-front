import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
//import { MatDialog } from '@angular/material';
//import { MatDialogConfig, MatDialogModule } from '@angular/material/dialog';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { AnnonceComponent } from './annonce/annonce.component';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { AnnonceService } from './services/annonce.service';
import { DetailsComponent } from './details/details.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginService } from './services/login.service';
import { NavbarComponent } from './navbar/navbar.component';
import { UserComponent } from './user/user.component';
import { AdminComponent } from './admin/admin.component';


const appRoutes: Routes = [
  
  {path:'login',component: LoginComponent},
  {path:'annonce',component: AnnonceComponent},
  {path:'admin',component: AdminComponent},
  {path:'user',component: UserComponent},
  {path:'details',component: DetailsComponent},
  {path:'',
redirectTo:'/login',
pathMatch:'full'}
]

@NgModule({

  declarations: [
    AppComponent,
    LoginComponent,
    AnnonceComponent,
    DetailsComponent,
    NavbarComponent,
    UserComponent,
    AdminComponent,
    
    
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
  ],

  providers: [AnnonceService,
    LoginService
  ],

  bootstrap: [AppComponent
  ],

  //entryComponents:[DetailsComponent]

})
export class AppModule { }
