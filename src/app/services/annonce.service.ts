import { Injectable } from '@angular/core';
import { HttpClient,} from '@angular/common/http';;
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Response } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class AnnonceService {
  
  idAnnonce:number;

  constructor(private http:HttpClient) { }
  
  private extractData(res: Response){
    const body = res;
    return body || {};
  }

 
  addAnnonce(annonce):Observable<any>{
    return this.http.post('http://localhost:8080/annonce/addannonce',annonce).pipe(map(this.extractData));    
  }
  getAnnonceById(id:number):Observable<any>{
    return this.http.get('http://localhost:8080/annonce/getannoncebyid/'+id,).pipe(map(this.extractData));
  }

  getAllAnnonces():Observable<any>{
    return this.http.get('http://localhost:8080/annonce/getallannonces').pipe(map(this.extractData));    
  }

  getAllCategories():Observable<any>{
    return this.http.get('http://localhost:8080/categorie/getallcategories').pipe(map(this.extractData));    
  }

  getAllRegions():Observable<any>{
    return this.http.get('http://localhost:8080/region/getallregions').pipe(map(this.extractData));    
  }

  getAllAnnoncesbyCat(nom:string):Observable<any>{
    return this.http.get('http://localhost:8080/annonce/getannoncesbynomcat/'+nom).pipe(map(this.extractData));    
  }
  getAllAnnoncesbyReg(nom:string):Observable<any>{
    return this.http.get('http://localhost:8080/annonce/getannoncesbynomreg/'+nom).pipe(map(this.extractData));    
  }
  getAllAnnoncesbyCatReg(nom1:string,nom2:string):Observable<any>{
    return this.http.get('http://localhost:8080/annonce/getannoncesbynomcatreg/'+nom1+'/'+nom2).pipe(map(this.extractData));    
  }
}
