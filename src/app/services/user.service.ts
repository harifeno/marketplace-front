import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders,} from '@angular/common/http';;
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Response } from 'selenium-webdriver/http';
@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }
  
  private extractData(res: Response){
    const body = res;
    return body || {};
  } 
  getAllUsers():Observable<any>{
    const headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json','Authorization' : localStorage.getItem('token')});
    return this.http.get('http://localhost:8080/gestion/getallusers',{headers}).pipe(map(this.extractData));
  }
  deleteUser(id):Observable<any>{
    const headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json','Authorization' : localStorage.getItem('token')});
    return this.http.delete('http://localhost:8080/gestion/deleteuserbyid/'+id,{headers}).pipe(map(this.extractData));
  }


}
