import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/catch';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Response } from 'selenium-webdriver/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  URL_API='http://localhost:8080';
  
  constructor(private http:HttpClient, private router:Router) { }

addUser(user):Observable<any>{
  //const headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json','Authorization' : localStorage.getItem('token')});
  return this.http.post('http://localhost:8080/gestion/adduser',user).pipe(map(this.extractData));
}
  
getOneUser(login:string):Observable<any>{
  return this.http.get('http://localhost:8080/gestion/getuserbylog/'+login).pipe(map(this.extractData));
}

authentication(login:string,password:string)
{
    return this.http.post(this.URL_API+'/login',{login:login,password:password},{observe:'response'})
}

private extractData(res: Response) {
  const body = res;
  return body || [];
}
}
