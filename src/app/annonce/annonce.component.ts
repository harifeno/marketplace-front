import { Component, OnInit } from '@angular/core';
import { AnnonceService } from '../services/annonce.service';
import * as _ from 'lodash';
import { Router } from '@angular/router';

@Component({
  selector: 'app-annonce',
  templateUrl: './annonce.component.html',
  styleUrls: ['./annonce.component.css']
})
export class AnnonceComponent implements OnInit {

  constructor(private annonceService:AnnonceService, private router:Router) { }

  annonces: []; annonce:{};
  categories: {};
  regions: {};
  nomReg:'';nomCat:'';

  //  appliquerFiltres(){
  //    this.annonces = _.filter(this.annonces,_.conforms(this.filtres))
  //  }
  //  filtrerNom(property:string, rule:any){
  //    this.filtres[property] = val => val==rule
  //    this.appliquerFiltres()
  //  }

  afficherAnnonces(){
    this.annonceService.getAllAnnonces().subscribe((data: []) => {
      this.annonces = data;
    }); }

  afficherCategories(){
    this.annonceService.getAllCategories().subscribe((data: {}) => {
      this.categories = data;
    }); }
  
  afficherRegions(){
      this.annonceService.getAllRegions().subscribe((data: {}) => {
      this.regions = data;
      }); }

  afficherParCategorie(){         
      this.annonceService.getAllAnnoncesbyCat(this.nomCat).subscribe(
      (data: []) => {this.annonces = data;},           
          ); }
  
  afficherParRegion(){
      this.annonceService.getAllAnnoncesbyReg(this.nomReg).subscribe(
      (data: []) => {this.annonces = data;},       
        ); }

  afficherParCategorieRegion(){
      this.annonceService.getAllAnnoncesbyCatReg(this.nomCat,this.nomReg).subscribe(
      (data: []) => {this.annonces = data;},       
        ); }
  
  rafraichir(){
    this.annonceService.getAllAnnonces().subscribe((data: []) => {
      this.annonces = data;
      res=>{this.afficherAnnonces()}
    }); 
  }
  
  afficherInfosAnnonce(id:number){
      this.annonceService.idAnnonce=id; 
      this.router.navigate(["/details"])
      console.log(this.annonceService.idAnnonce);
  }

  ngOnInit() {
this.annonceService.getAllCategories().subscribe((data: {}) => {
      this.categories = data;
    });
this.annonceService.getAllRegions().subscribe((data: {}) => {
      this.regions = data;
    });
this.annonceService.getAllAnnonces().subscribe((data: []) => {
      this.annonces = data;
    });

  }

}
